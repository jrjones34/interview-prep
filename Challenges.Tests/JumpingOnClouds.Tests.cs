using Challenges;
using System;
using Xunit;

namespace Challenges.Tests
{
    public class JumpingOnCloudsTests
    {
        [Fact]
        public void CallWithNoClouds_ReturnsZero()
        {
          int[] clouds = {};
          Assert.Equal(0, JumpingOnClouds.jumpingOnClouds(clouds));
        }

        [Fact]
        public void CallWithOnlyOneThunderheadCloud_ReturnsZero()
        {
          int[] clouds = {1};
          Assert.Equal(0, JumpingOnClouds.jumpingOnClouds(clouds));
        }

        [Fact]
        public void CallWith010_ReturnsOne()
        {
          int[] clouds = {0, 1, 0};
          Assert.Equal(1, JumpingOnClouds.jumpingOnClouds(clouds));
        }

        [Fact]
        public void CallWith0010_ReturnsTwo()
        {
          int[] clouds = {0, 0, 1, 0};
          Assert.Equal(2, JumpingOnClouds.jumpingOnClouds(clouds));
        }

        [Fact]
        public void CallWith0010010_ReturnsFour()
        {
          int[] clouds = {0, 0, 1, 0, 0, 1, 0};
          Assert.Equal(4, JumpingOnClouds.jumpingOnClouds(clouds));
        }

        [Fact]
        public void CallWith000100_ReturnsThree()
        {
          int[] clouds = {0, 0, 0, 1, 0, 0};
          Assert.Equal(3, JumpingOnClouds.jumpingOnClouds(clouds));
        }
    }
}
