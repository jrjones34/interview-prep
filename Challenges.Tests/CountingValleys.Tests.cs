using Challenges;
using System;
using Xunit;

namespace Challenges.Tests
{
    public class CountingValleysTests
    {
        [Fact]
        public void CallWithNoSteps_ReturnsZero()
        {
          int steps = 0;
          Assert.Equal(0, CountingValleys.countingValleys(steps, ""));
        }

        [Fact]
        public void CallWithOneStepDownAndOneStepUp_ReturnsOne()
        {
          int steps = 2;
          Assert.Equal(1, CountingValleys.countingValleys(steps, "DU"));
        }

        [Fact]
        public void CallWithOneStepUpAndOneStepDown_ReturnsZero()
        {
          int steps = 2;
          Assert.Equal(0, CountingValleys.countingValleys(steps, "UD"));
        }

        [Fact]
        public void CallWith2ValleysWithMixedCaseInput_ReturnsZero()
        {
          string steps = "DdUUuUDdDDuU";
          Assert.Equal(2, CountingValleys.countingValleys(steps.Length, steps));
        }

        [Fact]
        public void CallWith1Valley1Mountain_ReturnsOne()
        {
          string steps = "DDUUUUDD";
          Assert.Equal(1, CountingValleys.countingValleys(steps.Length, steps));
        }
    }
}
