using Challenges;
using System;
using Xunit;

namespace Challenges.Tests
{
    public class HourglassSumTests
    {
        [Fact]
        public void CallWithEmptyArrayOfArrays_ReturnsZero()
        {
          int[][] test = new int[6][];
          Assert.Equal(0, HourglassSum.maxHourglassSum(test));
        }

        [Fact]
        public void CallWith3x6AllOnes_Returns7()
        {
          int[][] test = new int[3][];
          for(int i = 0; i < 3; i++)
          {
            test[i] = new int[6] { 1, 1, 1, 1, 1, 1 };
          }
          Assert.Equal(7, HourglassSum.maxHourglassSum(test));
        }

        [Fact]
        public void CallWith6x6AllOnes_Returns7()
        {
          int[][] test = new int[6][];
          for(int i = 0; i < 6; i++)
          {
            test[i] = new int[6] { 1, 1, 1, 1, 1, 1 };
          }
          Assert.Equal(7, HourglassSum.maxHourglassSum(test));
        }

        [Fact]
        public void CallWith6x6AssortedValues_ReturnsCorrectValue()
        {
          int[][] test = new int[6][];
          test[0] = new int[6] { 1, 1, 1, 0, 0, 0 };
          test[1] = new int[6] { 0, 1, 0, 0, 0, 0 };
          test[2] = new int[6] { 1, 1, 1, 0, 0, 0 };
          test[3] = new int[6] { 0, 0, 2, 4, 4, 0 };
          test[4] = new int[6] { 0, 0, 0, 2, 0, 0 };
          test[5] = new int[6] { 0, 0, 1, 2, 4, 0 };

          Assert.Equal(19, HourglassSum.maxHourglassSum(test));
        }
    }
}
