using Challenges;
using System;
using Xunit;

namespace Challenges.Tests
{
    public class ArrayRotateLeftTests
    {
        [Fact]
        public void CallWithEmptyArrayAndNoShifts_ReturnsEmptyString()
        {
          int[] test = new int[0];
          int shifts = 0;
          Assert.Equal(String.Empty, ArrayRotateLeft.rotLeft(test, shifts));
        }

        [Fact]
        public void CallWithSingleElementArrayAndNoShifts_ReturnsEmptyString()
        {
          int[] test = new int[1] { 1 };
          int shifts = 0;
          Assert.Equal("1", ArrayRotateLeft.rotLeft(test, shifts));
        }

        [Fact]
        public void CallWithOneThroughFiveAndFourShifts_ReturnsCorrectResult()
        {
          int[] test = new int[5] { 1, 2, 3, 4, 5 };
          int shifts = 4;
          string result = "5 1 2 3 4";

          Assert.Equal(result, ArrayRotateLeft.rotLeft(test, shifts));
        }
    }
}
