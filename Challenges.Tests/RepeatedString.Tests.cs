using Challenges;
using System;
using Xunit;

namespace Challenges.Tests
{
    public class RepeatedStringTests
    {
        [Fact]
        public void CallWithEmptyString_ReturnsZero()
        {
          Assert.Equal(0, RepeatedString.repeatedString(String.Empty, 1));
        }

        [Fact]
        public void CallWithOnlyA5_ReturnsFive()
        {
          Assert.Equal(5, RepeatedString.repeatedString("A", 5));
        }

        [Fact]
        public void CallWithOnlyA1000000000000_Returns1000000000000()
        {
          Assert.Equal(1000000000000, RepeatedString.repeatedString("a", 1000000000000));
        }
    }
}
