using Challenges;
using System;
using Xunit;

namespace Challenges.Tests
{
    public class SockMerchantTests
    {
        [Fact]
        public void CallWithNoSocks_ReturnsZero()
        {
          int[] sockColors = new int[0];
          Assert.Equal(0, SockMerchant.sockMerchant(sockColors.Length, sockColors));
        }
        
        [Fact]
        public void CallWith1Sock_ReturnsZero()
        {
          int[] sockColors = new int[1]{1};
          Assert.Equal(0, SockMerchant.sockMerchant(1, sockColors));
        }
        
        [Fact]
        public void CallWithUnmatchedPair_ReturnsZero()
        {
          int[] sockColors = new int[2]{1, 2};
          Assert.Equal(0, SockMerchant.sockMerchant(sockColors.Length, sockColors));
        }
        
        [Fact]
        public void CallWithMatchingPairOnly_ReturnsOne()
        {
          int[] sockColors = new int[2]{1, 1};
          Assert.Equal(1, SockMerchant.sockMerchant(sockColors.Length, sockColors));
        }
        
        [Fact]
        public void CallWithMatchingPairPlusOne_ReturnsOne()
        {
          int[] sockColors = new int[3]{1, 2, 1};
          Assert.Equal(1, SockMerchant.sockMerchant(sockColors.Length, sockColors));
        }
        
        [Fact]
        public void CallWithAllSortsOfSocks_ReturnsCorrectValue()
        {
          int[] sockColors = new int[7]{1, 2, 1, 2, 1, 3, 2};
          Assert.Equal(2, SockMerchant.sockMerchant(sockColors.Length, sockColors));

          sockColors = new int[9]{10, 20, 20, 10, 10, 30, 50, 10, 20};
          Assert.Equal(3, SockMerchant.sockMerchant(sockColors.Length, sockColors));
        }
    }
}
