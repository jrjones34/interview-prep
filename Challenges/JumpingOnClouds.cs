using System;
using System.Collections;

namespace Challenges
{
    public class JumpingOnClouds
    {
        public static int jumpingOnClouds(int[] c)
        {
          if (c.Length < 2) return 0;


          int jumps = 0;
          int idx = 1; // Start on first cloud.
          
          while(idx < c.Length)
          {
            jumps++;
            idx += numberOfCloudsJumped(idx, c);
          }

          return jumps;
        }

        private static int numberOfCloudsJumped(int i, int[] clouds)
        {
          if(i + 1 < clouds.Length && clouds[i + 1] == 0)
          {
            return 2;
          }
          
          return 1;
        }
    }
}
