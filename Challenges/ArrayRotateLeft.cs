using System;
using System.Collections;
using System.Text;

namespace Challenges
{
    public class ArrayRotateLeft
    {
        public static string rotLeft(int[] a, int d)
        {
          if(a.Length == 0) return String.Empty;

          StringBuilder builder = new StringBuilder();
          builder.Append(buildBeginningOfString(a, d));
          if(d > 0) builder.Append(buildEndOfString(a, d));

          return builder.ToString().Trim();
        }

        private static string buildBeginningOfString(int[] a, int d)
        {
          StringBuilder builder = new StringBuilder();
          for(int i = d; i < a.Length; i++)
          {
            builder.Append(a[i].ToString() + " ");
          }

          return builder.ToString();
        }

        private static string buildEndOfString(int[] a, int d)
        {
          StringBuilder builder = new StringBuilder();
          for(int i = 0; i < d; i++)
          {
            builder.Append(a[i].ToString() + " ");
          }

          return builder.ToString();
        }
    }
}
