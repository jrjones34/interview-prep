using System;
using System.Collections;

namespace Challenges
{
    public class CountingValleys
    {
        public static int countingValleys(int n, string s)
        {
          if(n == 0) return 0;

          int altitude = 0;
          int valleys = 0;
          
          string temp = s.ToUpper();
          for(int i = 0; i < temp.Length; i++)
          {
            if (isStepDown(temp[i]))
            {
              altitude--;
            }
            else // Assume 'U'
            {
              altitude++;
              
              if (atSeaLevel(altitude))
              {
                valleys++;
              }
            }
          }

          return valleys;
        }

        private static bool isStepDown(char step)
        {
          if (step == 'D')
          {
            return true;
          }
          //
          return false;
        }

        private static bool atSeaLevel(int altitude)
        {
          return altitude == 0;
        }
    }
}
