using System;
using System.Collections;

namespace Challenges
{
    public class SockMerchant
    {
        public static int sockMerchant(int n, int[] ar)
        {
          if (n < 2) return 0;

          Hashtable socks = new Hashtable();
          int pairsToSell = 0;

          for (int i = 0; i < ar.Length; i++)
          {
            int color = ar[i];
            if (socks.ContainsKey(color))
            {
              pairsToSell++;
              socks.Remove(color);
            }
            else
            {
              socks.Add(color, 1);
            }
          }
          
          return pairsToSell;
        }
    }
}
