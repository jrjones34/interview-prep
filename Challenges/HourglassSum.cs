using System;
using System.Collections;

namespace Challenges
{
    public class HourglassSum
    {
        public static int maxHourglassSum(int[][] ar)
        {
          if(ar.Length == 0 || ar[0] == null || ar[0].Length == 0) return 0;

          int maxSum = Int32.MinValue;

          for(int row = 0; row < ar.Length - 2; row++)
          {
            for(int col = 0; col < ar[row].Length - 2; col++)
            {
              int total = sumOfCurrentHourglass(ar, row, col);
              maxSum = setMaxSum(maxSum, total);
            }
          }

          return maxSum;
        }

        private static int sumOfCurrentHourglass(int[][] ar, int row, int col)
        {
          return ar[row][col] + ar[row][col + 1] + ar[row][col + 2]
                 + ar[row + 1][col + 1]
                 + ar[row + 2][col] + ar[row + 2][col + 1] + ar[row + 2][col + 2];
        }

        private static int setMaxSum(int currentMax, int challenger)
        {
          if (challenger > currentMax)
            return challenger;
          // else
          return currentMax;
        }
    }
}
