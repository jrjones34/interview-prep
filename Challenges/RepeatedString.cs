using System;
using System.Collections;
using System.Linq;

namespace Challenges
{
    public class RepeatedString
    {
        public static long repeatedString(string s, long n)
        {
          string temp = s.ToLower();
          long occurrencesOfA = getOccurrencesOfAInString(temp);
          if(occurrencesOfA == 0) return 0;

          long timesToRepeat = getTimesToRepeatFullInitialString(n, temp.Length);
          occurrencesOfA *= timesToRepeat;

          int lengthOfRemainderString = getLengthOfRemainderString(n, temp.Length);
          if (lengthOfRemainderString > 0)
          {
            occurrencesOfA += temp.Substring(0, lengthOfRemainderString).Count(c => c == 'a');
          }

          return occurrencesOfA;
        }

        private static long getOccurrencesOfAInString(string s)
        {
          return s.Count(c => c == 'a');
        }

        private static long getTimesToRepeatFullInitialString(long finalStringLength, double initialStringLength)
        {
          return Convert.ToInt64(Math.Floor(finalStringLength / initialStringLength));
        }

        private static int getLengthOfRemainderString(long finalStringLength, long initialStringLength)
        {
          return Convert.ToInt32(finalStringLength % initialStringLength);
        }

        private static long getOccurrencesOfAInRemainderString(string initialString, int lengthOfRemainderString)
        {
          return getOccurrencesOfAInString(initialString.Substring(0, lengthOfRemainderString));
        }
    }
}
